import React from 'react';
import {Link, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';


//Redux
import { useDispatch } from 'react-redux';
import { borrarProductoAction, obtenerProductoEditar } from '../actions/productoActions';

const Producto = ({producto}) => {
    const { nombre, precio, id } = producto;
    const dispatch = useDispatch();
    const history = useHistory(); // habilitar history para redireccion

    // Confirmar si desea eliminarlo
    const confirmarEliminarProducto = id => {

        // Preguntamos al usuario
        Swal.fire({
            title: '¿Estás seguro?',
            text: "Un producto eliminado, no se puede recuperar",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar',
            cancelButtonText: 'Cancelar'
          }).then((result) => {
            if (result.value) {

                //pasarlo al action
                dispatch( borrarProductoAction(id) );
            }
          });

    }

    //función que redirige de forma programada
    const redireccionarEdición = producto => {
        dispatch(obtenerProductoEditar(producto));
        history.push(`/productos/editar/${producto.id}`)
    }

    return( 
        <tr>
            <td>{nombre}</td>
            <td className="font-weight-bold">$ {precio} </td>
            <td className="acciones">
                <button
                    type="button"  
                    onClick={()=> redireccionarEdición(producto)}
                    className="btn btn-primary mr-2">
                    Editar
                </button>
                <button
                    type="button"
                    className="btn btn-danger"
                    onClick={()=> confirmarEliminarProducto(id)}
                >Eliminar</button>
            </td>
        </tr>

    );
}

export default Producto;