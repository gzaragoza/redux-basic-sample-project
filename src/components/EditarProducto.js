import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { editarProductoAction } from '../actions/productoActions';
import { useHistory } from 'react-router-dom';

const EditarProducto = () => {

    const history = useHistory();
    const dispatch = useDispatch();

    //nuevo state de producto
    const [producto, guardarProducto] = useState({
        nombre: '',
        precio: ''
    })


    const productoeditar = useSelector(state => state.productos.productoeditar);
   
    useState( ()=> {
        guardarProducto(productoeditar);
    },[productoeditar]);

    //leer los datos del formulario
    const onChangeFormulario = e => {
        guardarProducto({
            ...producto,
            [e.target.name] : e.target.value
        })
    }

    const { nombre, precio } = producto; 

    const submitEditarProducto = e => {
        e.preventDefault();
       dispatch(editarProductoAction(producto));
       history.push('/');
    }

    return(
        <div className="row justify-content-center">
           <div className="col-md-8">
               <div className="card">
                   <div className="card-body">
                       <h2 className="text-center mb-4 font-weight-bold">
                           Editar Producto
                       </h2>

                        <form
                            onSubmit={submitEditarProducto}
                        >
                            <div className="form-group mt-5">
                                <label>Nombre de Producto</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Nombre de producto"
                                    name="nombre"
                                    value={nombre}
                                    onChange={onChangeFormulario}
                                />
                            </div>
                            <div className="form-group mt-4">
                                <label>Precio de Producto</label>
                                <input
                                    type="number"
                                    className="form-control"
                                    placeholder="Precio producto"
                                    name="precio"
                                    value={precio}
                                    onChange={onChangeFormulario}
                                />
                            </div>

                            <button 
                                type="submit"
                                className="btn btn-primary font-weight-bold 
                                text-uppercase b-block w-100 mt-4"
                            >
                                Guardar Cambios
                            </button>                   
                             </form>

                   </div>
               </div>
           </div>
       </div>
    );
}

export default EditarProducto;