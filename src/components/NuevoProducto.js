import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

//actions de Redux
import { crearNuevoProductoAction } from '../actions/productoActions';

const NuevoProducto = ({history}) => {

    //stade del componente
    const [nombre, guardarNombre] = useState('');
    const [precio, guardarPrecio] = useState(0);
    

    //utilizar use dispatch y te crea una función
    const dispatch = useDispatch();

    //acceder al state del store
    const cargando = useSelector( state => state.productos.loading);
    const error = useSelector(state => state.productos.error);

    // mandar llamar el action de productoAction
    const agregarProducto = (producto) => dispatch(crearNuevoProductoAction(producto));

    // cuando e usuario haga submit
    const submitNuevoProducto = e => {
        e.preventDefault();

        //Validar formulario
        if(nombre.trim() ==='' || precio.trim() <= 0) {
            return;
        }

        // si no hay errores

        // crear nuevo producto
        agregarProducto({
            nombre,
            precio
        });

        history.push('/'); 
    }

    return(
       <div className="row justify-content-center">
           <div className="col-md-8">
               <div className="card">
                   <div className="card-body">
                       <h2 className="text-center mb-4 font-weight-bold">
                           Agregar Nuevo Producto
                       </h2>

                        <form
                            onSubmit={submitNuevoProducto}
                        >
                            <div className="form-group mt-5">
                                <label>Nombre de Producto</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Nombre de producto"
                                    name="nombre"
                                    value={nombre}
                                    onChange={e => guardarNombre(e.target.value)}
                                />
                            </div>
                            <div className="form-group mt-4">
                                <label>Precio de Producto</label>
                                <input
                                    type="number"
                                    className="form-control"
                                    placeholder="Precio producto"
                                    name="precio"
                                    value={precio}
                                    onChange={e => guardarPrecio(e.target.value)}
                                />
                            </div>

                                <button 
                                    type="submit"
                                    className="btn btn-primary font-weight-bold 
                                    text-uppercase b-block w-100 mt-4"
                                >
                                    Agregar Producto
                                </button>  
                                { cargando ? <p>Cargando...</p>: null }    
                                { error ? <p className="alert alert-danger p2 
                                    text-center">Hubo un error</p> : null}             
                            </form>

                   </div>
               </div>
           </div>
       </div>
    );
}

export default NuevoProducto;